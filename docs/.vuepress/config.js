module.exports = {
  head: [
    ['link', { rel: 'icon', href: '/PBST-Logo.png' }],
    ['link', { rel: 'manifest', href: '/manifest.json' }],
    ['meta', { name: 'theme-color', content: '#9e1d1d' }],
  ],
  dest: 'public/',
  title: 'PET Handbook',
  description: 'The official PET Handbook',
  themeConfig: {
    sidebarDepth: 4,
    repo: 'https://gitlab.com/pinewood-builders/PET-Handbook',
    editLinks: true,
    yuu: {
      defaultDarkTheme: true,
      defaultColorTheme: 'green',
      disableThemeIgnore: true,
    },
    docsDir: 'docs/',
    logo: '/PBST-Logo.png',
    smoothScroll: true,
    nav: [{
      text: 'Home',
      link: '/'
    },
    {
      text: 'Divisions',
      items: [
        {
          text: 'PET-Handbook',
          link: '/pet/'
        },
      {
        text: 'Fire-Handbook',
        link: '/pet/fire/'
      },
      {
        text: 'Hazmat-Handbook',
        link: '/pet/hazmat/'
      },{
        text: 'Medic-Handbook',
        link: '/pet/medic/'
      },
      ]
    },
    {
      text: 'Pinewood',
      items: [{
          text: 'PBST-Handbook',
          link: 'https://pbst.pinewood-builders.com'
        },
        {
          text: 'TMS-Handbook',
          link: 'https://tms.pinewood-builders.com'
        }
      ]
    }
  ],
  sidebar: {
    '/pet/': [
        'medic/',
        'hazmat/',
        'fire/'
    ]
  },
  
  },
  
 
  plugins: [
    ['@vuepress/pwa',
      {
        serviceWorker: true,
        updatePopup: true
      }
    ],
  ]
}
